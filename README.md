# (HA)²proxy

### An Ansible swiss-army-knife to configure Highly Available HAProxy


This toolkit aims to provide a reliable and easy way to set up
and maintain a cluster of HAProxy proxies but can also work
with a single node.

# Infrastructure

## Minimum setup

A single host with public IP address used as primary.

## Suggested setup

- 1x HAProxy primary host
- 1 or more HAProxy secondary host(s)


## Sample domains.yml file

```yaml
DOMAINS:
  # HTTPS backend
  - domain: zbx-scuole.fuss.bz.it
    servers:
      - zbx-scuole.private.fuss.bz.it:443
    ssl:
      enabled: yes
      verify: yes
  # Plain HTTP backend
  - domain: status.fuss.bz.it
    servers:
      - private.fuss.bz.it:8070
    restrictions:
      - type: http-request
        conditions: 
          - type: path 
            value: /web
            flags: "-i -m beg"
          - type: src 
            value:  192.168.1.1
            negate: true
```

